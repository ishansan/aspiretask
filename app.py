from flask import Flask, redirect, render_template, url_for, session,request
from flask_oauth import OAuth
from flask.ext.mysql import MySQL
import json

GOOGLE_CLIENT_ID = '942039119780-jhjotabvo9t0rlsicumjuk9nppd5k6tk.apps.googleusercontent.com'
GOOGLE_CLIENT_SECRET = 'qfLYBesWviKhkvRH4WSEtXI4'
REDIRECT_URI = '/oauth2callback'
SECRET_KEY = 'development key'
DEBUG = True

app = Flask(__name__)
mysql = MySQL()
# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'root'
app.config['MYSQL_DATABASE_DB'] = 'aspire'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)
conn = mysql.connect()
cursor = conn.cursor()

app.debug = DEBUG
app.secret_key = SECRET_KEY # secret key - session
oauth = OAuth()
google = oauth.remote_app('google',
                          base_url='https://www.google.com/accounts/',
                          authorize_url='https://accounts.google.com/o/oauth2/auth',
                          request_token_url=None,
                          request_token_params={'scope': 'https://www.googleapis.com/auth/userinfo.email',
                                                'response_type': 'code'},
                          access_token_url='https://accounts.google.com/o/oauth2/token',
                          access_token_method='POST',
                          access_token_params={'grant_type': 'authorization_code'},
                          consumer_key=GOOGLE_CLIENT_ID,
                          consumer_secret=GOOGLE_CLIENT_SECRET)

@app.route('/')
def main():
    return render_template('index.html')

@app.route('/sendbio')
def sendbio():
    get_email = str(session["email"])
    query = ("SELECT * FROM aspirelist WHERE email='ishansan38@gmail.com'")
    # data = (get_email)
    cursor.execute(query)
    rows = cursor.fetchall()
    # for row in rows:
    return json.dumps(rows)
    # return get_email

@app.route('/account')
def account():
    if(len(session)>0):
      query = "SELECT * FROM aspirelist"
      result = cursor.execute(query)
      return render_template('account.html',fires=query)
    else:
      return "You are not Logged in"  
@app.route('/app')
def index():
    
    access_token = session.get('access_token')
    if access_token is None:
        return redirect(url_for('login'))
 
    access_token = access_token[0]
    from urllib2 import Request, urlopen, URLError
 
    headers = {'Authorization': 'OAuth '+access_token}
    req = Request('https://www.googleapis.com/oauth2/v1/userinfo',
                  None, headers)
    try:
        res = urlopen(req)

    except URLError, e:
        if e.code == 401:
            # Unauthorized - bad token
            session.pop('access_token', None)
            return redirect(url_for('login'))
        return res.read()
 
    json_det = json.loads(res.read()) # GET DETAILS FROM JSON
    get_email = json_det["email"]

    #set session for this Email
    session['email'] = get_email
    # insert json_det into MYSQL
    insert_stmt = ("INSERT INTO aspirelist (email) VALUES (%s)")
    data = (json_det)
    cursor.execute(insert_stmt,get_email)
    conn.commit()
    # return page 
    return redirect('/account')

@app.route('/printsession')
def sess():
    return session["email"]

@app.route('/getsession',methods=['GET'])
def sesstwo():
    get_email = session["email"]
    data = {"email":get_email}
    return json.dumps(data);

@app.route('/postbio',methods=['POST'])
def bio():
    bio = request.form["text_bio"]
    get_email = session["email"]
    insert_stmt = ("UPDATE aspirelist SET bio=%s WHERE email=%s")
    data = (bio,get_email)
    cursor.execute(insert_stmt,data)
    conn.commit()

    return render_template('account.html')
    

@app.route('/login')
def login():
    callback=url_for('authorized', _external=True)
    return google.authorize(callback=callback)

@app.route('/logout')
def logout():
    session.clear()
    return render_template('index.html') 
 
@app.route(REDIRECT_URI)
@google.authorized_handler
def authorized(resp):
    access_token = resp['access_token']
    session['access_token'] = access_token, ''
    return redirect(url_for('index'))
 
 
@google.tokengetter
def get_access_token():
    return session.get('access_token')
 
 
def main():
    app.run()
 
 
if __name__ == '__main__':
    main()